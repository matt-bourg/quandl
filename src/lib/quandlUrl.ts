import apiKey from './apiKey';
import { IQuandlData, IQuandlDataQueryUrl } from './interfaces';

function getUrl(data: IQuandlData): IQuandlDataQueryUrl {
	// Pattern: https://data.nasdaq.com/api/v3/datatables/QUOTEMEDIA/PRICES.json?ticker=%s&date.gte=%s&date.lte=%s&api_key=%s
	// and for ticker info: https://data.nasdaq.com/api/v3/datatables/QUOTEMEDIA/TICKERS?ticker=%s&api_key=%s
	if (data.ticker === '') {
		return null;
	}

	let urlRoot: string, key: string;

	if (process.env.NODE_ENV === 'production') {
		urlRoot = 'https://data.nasdaq.com/api/v3/datatables/QUOTEMEDIA/';
		key = 'api_key=' + apiKey.key;
	} else {
		urlRoot = 'http://localhost:3000/';
		key = 'api_key=blahblah';
	}

	const ticker = 'ticker=' + data.ticker + '&';
	const startDate =
		data.startDate !== '' ? 'date.gte=' + data.startDate + '&' : '';
	const endDate = data.endDate !== '' ? 'date.lte=' + data.endDate + '&' : '';
	const quotesUrl =
		urlRoot + 'PRICES.json?' + ticker + startDate + endDate + key;
	const tickerInfoUrl = urlRoot + 'TICKERS.json?' + ticker + key;

	return {
		tickerInfo: {
			name: 'tickerInfo',
			url: tickerInfoUrl
		},
		quotesData: {
			name: 'quotesData',
			url: quotesUrl
		}
	};
}

export default getUrl;
