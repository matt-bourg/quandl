import React, { Dispatch, SetStateAction } from 'react';

interface IQuoteContext {
	quandlData: IQuandlData;
	setQuandlData: (object: IQuandlData, override?: boolean) => void;
}

interface IQuandlQueryUrl {
	name: string;
	url: string;
}

interface IQuandlDataQueryUrl {
	tickerInfo: IQuandlQueryUrl;
	quotesData: IQuandlQueryUrl;
}

interface IColumn {
	name: string;
	type?: string;
}

type IQuotesDataRow = [
	string, // ticker
	string, // date
	number, // open
	number, // high
	number, // low
	number, // close
	number, // volume
	number, // dividend
	number, // split
	number, // adjusted open
	number, // adjusted high
	number, // adjusted low
	number, // adjusted close
	number // adjusted volume
];

interface IQuotesData {
	columns: IColumn[];
	data: IQuotesDataRow[];
	name: string;
	url: string;
}

interface ITickerInfo {
	columns: IColumn[];
	data: string[];
}

interface IQuandlData {
	ticker?: string;
	startDate?: string;
	endDate?: string;
	tickerInfo?: ITickerInfo;
	quotesData?: IQuotesData;
}

interface IFormProps {
	quandlData: IQuandlData;
	setQuandlData: (object: IQuandlData) => void;
	handleChange: React.ChangeEventHandler<HTMLInputElement>;
	handleSubmit: React.FormEventHandler<HTMLFormElement>;
	handleReset: React.FormEventHandler<HTMLFormElement>;
}

interface ITableColumns {
	columns: IColumn[];
}

interface IRowHeader {
	referenceDate: string;
}

interface IRowValues {
	values: number[];
}

interface ITableRow {
	referenceDate: string;
	values: number[];
}

interface ITableBody {
	data: IQuotesDataRow[];
}

interface ITableProps {
	quandlData: IQuandlData;
}

interface ICandlesticksProps {
	quandlData: IQuandlData;
}

interface ICandlesticksDataRow {
	date: string;
	open: number;
	high: number;
	low: number;
	close: number;
}

export {
	IQuoteContext,
	IQuandlQueryUrl,
	IQuandlDataQueryUrl,
	IQuandlData,
	IFormProps,
	IRowHeader,
	IRowValues,
	ITableRow,
	ITableColumns,
	ITableBody,
	ITableProps,
	ICandlesticksProps,
	ICandlesticksDataRow
};
