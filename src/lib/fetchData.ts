import axios from 'axios';
import { IQuandlQueryUrl } from './interfaces';

const handleServiceResponseSuccess = (res: any, data: IQuandlQueryUrl) => {
	if (res && res.data && res.data.datatable) {
		return { ...res.data.datatable, ...data };
	}
	throw 'Error: No data returned from service';
};

const handleServiceResponseError = (error: any) => {
	throw error;
};

const fetchData = (data: IQuandlQueryUrl) =>
	axios
		.get(data.url)
		.then((res) => handleServiceResponseSuccess(res, data))
		.catch((error) => handleServiceResponseError(error));

export { fetchData };
