// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getAnalytics, logEvent } from 'firebase/analytics';
import firebaseConfig from '../lib/firebaseConfig';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Initialize Firebase
const analyticsApp = initializeApp(firebaseConfig);
const analytics = getAnalytics(analyticsApp);
const fireEvent = (event: any, eventParams?: object) =>
	logEvent(analytics, event, eventParams);

export { fireEvent };
