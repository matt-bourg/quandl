import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from './components/ErrorBoundary';
import App from './App';
import './less/index.less';

ReactDOM.render(
	<StrictMode>
		<ErrorBoundary>
			<App />
		</ErrorBoundary>
	</StrictMode>,
	document.getElementById('root')
);
