import { useState, useEffect } from 'react';
import { IQuandlData, IQuoteContext } from '../lib/interfaces';

function useQuandl(): IQuoteContext {
	const initialQuandlData: IQuandlData = {
		ticker: '',
		startDate: '',
		endDate: ''
	};

	const [quandlData, setData] = useState<IQuandlData>(() => {
		try {
			return (
				JSON.parse(localStorage.getItem('quandlData')) ?? initialQuandlData
			);
		} catch (error) {
			console.error('Wrong data in localStorage');
			return initialQuandlData;
		}
	});

	useEffect(() => {
		localStorage.setItem('quandlData', JSON.stringify(quandlData));
		document.title =
			'Quandl ' + (quandlData.ticker ? '- ' + quandlData.ticker : '');
	}, [quandlData]);

	const setQuandlData = (object: IQuandlData, override?: boolean) => {
		if (override) {
			setData(object);
		} else {
			setData((previousData: IQuandlData) => ({ ...previousData, ...object }));
		}
	};

	return {
		quandlData,
		setQuandlData
	};
}

export { useQuandl };
