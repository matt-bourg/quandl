import { useContext } from 'react';
import { QuoteContext } from '../contexts/QuoteContext';

export const useQuoteContext = () => useContext(QuoteContext);
