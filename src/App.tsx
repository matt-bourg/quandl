import React from 'react';
import QuandlContainer from './components/QuandlContainer';

function App(): JSX.Element {
	return <QuandlContainer />;
}

export default App;
