import React, { ReactNode, createContext } from 'react';
import { useQuandl } from '../hooks';
import { IQuoteContext } from '../lib/interfaces';

const QuoteContext = createContext<IQuoteContext>(undefined);

type providerProps = {
	children: ReactNode;
};

const QuoteContextProvider = (props: providerProps): JSX.Element => {
	const { children } = props;
	const { quandlData, setQuandlData } = useQuandl();

	return (
		<QuoteContext.Provider
			value={{
				quandlData,
				setQuandlData
			}}
		>
			{children}
		</QuoteContext.Provider>
	);
};

export { QuoteContextProvider, QuoteContext };
