import React from 'react';
import { useQuoteContext } from '../hooks';
import '../less/QuandlTable.less';
import {
	ITableColumns,
	IRowHeader,
	IRowValues,
	ITableRow,
	ITableBody,
	ITableProps
} from '../lib/interfaces';

function TableHeaders(props: ITableColumns): JSX.Element {
	const { columns } = props;
	const capitalizeName = (name: string) => {
		const [firstLetter, ...rest] = [...name];
		return [firstLetter.toUpperCase(), ...rest].join('');
	};
	const headers = columns.map((column) => (
		<th key={column.name}>{capitalizeName(column.name)}</th>
	));
	return <>{headers}</>;
}

function TableRowHeader(props: IRowHeader): JSX.Element {
	return <th>{props.referenceDate}</th>;
}

function TableRowValues(props: IRowValues): JSX.Element {
	const { values } = props;
	const rowValues = values.map((cell, index) => (
		<td key={index}>
			{cell.toLocaleString('en-US', { maximumFractionDigits: 2 })}
		</td>
	));
	return <>{rowValues}</>;
}

function TableRow(props: ITableRow): JSX.Element {
	const { referenceDate, values } = props;
	return (
		<tr>
			<TableRowHeader referenceDate={referenceDate} />
			<TableRowValues values={values} />
		</tr>
	);
}

function TableBody(props: ITableBody): JSX.Element {
	const rows = props.data;
	const bodyRows = rows.map((row: any) => {
		const referenceDate = row[1];
		const values = row.slice(2, 7);

		return (
			<TableRow
				key={referenceDate}
				referenceDate={referenceDate}
				values={values}
			/>
		);
	});
	return <>{bodyRows}</>;
}

function Table(props: ITableProps): JSX.Element {
	const quotesData = props.quandlData && props.quandlData.quotesData;
	const dataset = quotesData && quotesData.data;
	const columns = quotesData && quotesData.columns.slice(1, 7);
	const tickerInfo = props.quandlData && props.quandlData.tickerInfo;
	const tickerName = tickerInfo && tickerInfo.data && tickerInfo.data[0][2];

	if (dataset && columns) {
		return (
			<>
				<div className="content">
					<h4>{tickerName}</h4>
				</div>
				<table className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
					<thead>
						<tr>
							<TableHeaders columns={columns} />
						</tr>
					</thead>
					<tbody>
						<TableBody data={dataset} />
					</tbody>
				</table>
			</>
		);
	}
	return <p>No data</p>;
}

function QuandlTable(): JSX.Element {
	const { quandlData } = useQuoteContext();

	return (
		<div className="column is-full">
			<div className="box">
				<div className="table-container">
					<Table quandlData={quandlData} />
				</div>
			</div>
		</div>
	);
}

export { QuandlTable };
