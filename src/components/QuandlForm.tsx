import React, { SyntheticEvent } from 'react';
import { useQuoteContext } from '../hooks';
import { IQuandlData } from '../lib/interfaces';
import '../less/QuandlForm.less';
import getUrl from '../lib/quandlUrl';
import { fetchData } from '../lib/fetchData';
import { fireEvent } from '../lib/analytics';

function QuandlForm(): JSX.Element {
	const { quandlData, setQuandlData } = useQuoteContext();

	const initialQuandlData: IQuandlData = {
		ticker: '',
		startDate: '',
		endDate: ''
	};

	const getData = () => {
		const tickerData = getUrl(quandlData);
		const { tickerInfo, quotesData } = tickerData;
		if (tickerInfo.url && quotesData.url) {
			Promise.all([fetchData(tickerInfo), fetchData(quotesData)])
				.then((resultsets) => {
					resultsets.forEach((resultset) => {
						setQuandlData({ [resultset.name]: resultset });
					});
				})
				.catch((error) => {
					console.log(error);
				});
		} else {
			console.log('No service URL provided');
		}
	};

	const handleChange = (event: SyntheticEvent) => {
		if (event) {
			const target = event.target as HTMLTextAreaElement;
			setQuandlData({
				[target.name]: target.value
			});
		}
	};

	const handleSubmit = (event: SyntheticEvent) => {
		if (event) {
			event.preventDefault();
		}
		getData();
		fireEvent('getQuote', {
			ticker: quandlData.ticker,
			startDate: quandlData.startDate,
			endDate: quandlData.endDate
		});
	};

	const handleReset = () => {
		setQuandlData(initialQuandlData, true);
	};

	const isDateRangeOk = () => {
		const maxDateRange = 365 * 2;
		const date1 = new Date(quandlData.startDate);
		const date2 =
			quandlData.endDate === '' ? Date.now() : new Date(quandlData.endDate);
		const day = 1000 * 60 * 60 * 24;

		return (
			date2 > date1 && (date2.valueOf() - date1.valueOf()) / day <= maxDateRange
		);
	};

	return (
		<div
			className={`column ${
				quandlData.quotesData ? 'is-4' : 'is-6 is-offset-3'
			}`}
		>
			<div className="box">
				<form onSubmit={handleSubmit} onReset={handleReset}>
					<div className="field">
						<label className="label">Quandl ticker</label>
						<div className="control">
							<input
								className="input"
								type="text"
								name="ticker"
								onChange={handleChange}
								value={quandlData.ticker}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">Start date</label>
						<div className="control">
							<input
								className="input"
								type="date"
								name="startDate"
								onChange={handleChange}
								value={quandlData.startDate}
							/>
						</div>
					</div>
					<div className="field">
						<label className="label">End date</label>
						<div className="control">
							<input
								className="input"
								type="date"
								name="endDate"
								onChange={handleChange}
								value={quandlData.endDate}
							/>
						</div>
					</div>
					<div className="buttons-container is-fullwidth is-flex is-flex-direction-row">
						<button
							type="reset"
							className="button is-info is-outlined is-fullwidth"
						>
							Reset
						</button>
						<button
							type="submit"
							className="button is-info is-fullwidth"
							disabled={quandlData.ticker === '' || !isDateRangeOk()}
						>
							Get quote
						</button>
					</div>
				</form>
			</div>
		</div>
	);
}

export { QuandlForm };
