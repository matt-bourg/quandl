import React from 'react';
import 'bulma/css/bulma.css';
import { QuandlForm, QuandlTable, CandleSticks } from './';
import { QuoteContextProvider } from '../contexts/QuoteContext';

function QuandlApp(): JSX.Element {
	return (
		<div className="section is-fullheight">
			<div className="container">
				<div className="columns">
					<QuandlForm />
					<CandleSticks />
				</div>
				<div className="columns">
					<QuandlTable />
				</div>
			</div>
		</div>
	);
}

function QuandlContainer(): JSX.Element {
	return (
		<QuoteContextProvider>
			<QuandlApp />
		</QuoteContextProvider>
	);
}

export default QuandlContainer;
