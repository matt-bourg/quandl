import React, { useEffect } from 'react';
import * as d3 from 'd3';
import { ICandlesticksDataRow } from '../lib/interfaces';
import { useQuoteContext } from '../hooks';

const candleStickDraw = (data: ICandlesticksDataRow[]) => {
	const margin = {
		top: 10,
		right: 40,
		bottom: 20,
		left: 40
	};
	const width = 800 - margin.left - margin.right;
	const height = 400 - margin.top - margin.bottom;
	const colors = ['green', 'black', 'red']; // [up, no change, down]

	const dates = data.map((d) => d.date);
	const numberedDates = dates.map((date) => new Date(date));
	const dataLength = dates.length;
	const lows = data.map((d) => d.low);
	const highs = data.map((d) => d.high);

	const [minQuote, maxQuote] = [d3.min(lows), d3.max(highs)];
	const adjRate = 0.01;
	const [adjMinQuote, adjMaxQuote] = [
		(1 - adjRate) * minQuote,
		(1 + adjRate) * maxQuote
	];

	const scaleValues = (value: number) =>
		height - ((value - adjMinQuote) * height) / (adjMaxQuote - adjMinQuote);

	const scaledOpens = data.map((d) => d.open).map(scaleValues);
	const scaledCloses = data.map((d) => d.close).map(scaleValues);
	const scaledHighs = highs.map(scaleValues);
	const scaledLows = lows.map(scaleValues);

	d3.select('svg').remove(); // Needs to be removed for each rendering from React

	const svg = d3
		.select('#candleStickChart')
		.append('svg')
		.attr('width', width + margin.left + margin.right)
		.attr('height', height + margin.top + margin.bottom)
		.attr('viewBox', [
			0,
			0,
			width + margin.left + margin.right,
			height + margin.top + margin.bottom
		])
		.attr('style', 'max-width: 100%; height: auto; height: intrinsic;')
		.append('g')
		.attr('transform', `translate(${margin.left},${margin.top})`);

	const x = d3
		.scaleLinear()
		.domain(numberedDates)
		.range(d3.range(dataLength).map((d, i) => (i * width) / (dataLength - 1)));

	const xAxis = d3.axisBottom(x).tickFormat(d3.utcFormat('%m/%d'));

	svg.append('g').attr('transform', `translate(0,${height})`).call(xAxis);

	const y = d3
		.scaleLinear()
		.domain([adjMinQuote, adjMaxQuote])
		.range([height, 0]);

	svg.append('g').call(d3.axisLeft(y));

	svg
		.selectAll('.tick')
		.append('line')
		.attr('stroke', 'black')
		.attr('opacity', 0.1)
		.attr('x2', width);

	const g = svg
		.append('g')
		.attr('stroke', 'black')
		.selectAll('g')
		.data(d3.range(dataLength))
		.join('g')
		.attr('transform', (i) => `translate(${(i * width) / (dataLength - 1)},0)`);

	g.append('line')
		.attr('y1', (d, i) => scaledLows[i])
		.attr('y2', (d, i) => scaledHighs[i]);

	const strokeWidth = Math.min(12, Math.floor(0.9 * (width / dataLength)));

	g.append('line')
		.attr('y1', (d, i) => scaledOpens[i])
		.attr('y2', (d, i) => scaledCloses[i])
		.attr('stroke-width', strokeWidth)
		.attr(
			'stroke',
			(d, i) => colors[1 + Math.sign(scaledCloses[i] - scaledOpens[i])]
		);
};

function CandleSticks(): JSX.Element {
	const { quandlData } = useQuoteContext();
	const data =
		quandlData.quotesData &&
		quandlData.quotesData.data.map((dataPoint) => ({
			date: d3.timeParse('%Y-%m-%d')(dataPoint[1]).toString(),
			open: dataPoint[2],
			high: dataPoint[3],
			low: dataPoint[4],
			close: dataPoint[5]
		}));

	if (!data) return null;

	useEffect(() => candleStickDraw(data.reverse()), [data]);
	return (
		<div className="column is-8">
			<div className="box">
				<div id="candleStickChart" />
			</div>
		</div>
	);
}

export { CandleSticks };
