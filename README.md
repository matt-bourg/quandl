### Quick comments

1. This app is set up with Webpack, React and D3.js (refer to webpack.config.js for this setup).

2. Refer to package.json for modules to install and run "npm install".

3. API key for quotes needs to obtained from [Quandl](https://data.nasdaq.com/) (now part of Nasdaq Data Link offering). Paid subscription for Quandl data is required but there are free keys for other data sources.

4. To run in local, run "npm start" along with "npm run serve" (for mocked responses, stored in db.json file).

### Example with AAPL

![Alt text](./doc/screenshot.png)
